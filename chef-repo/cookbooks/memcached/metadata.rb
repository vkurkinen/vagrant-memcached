maintainer       "F-Secure Corp"
maintainer_email "ville.kurkinen@f-secure.com"
license          "All rights reserved"
description      "Installs and configures simple memcached cluster"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"

recipe            "memcached::base"        , "install basic utils"
recipe            "memcached::server"      , "install memcached node"
recipe            "memcached::admin"       , "install memcached admin node"

%w{ ubuntu debian }.each do |os|
  supports os
end

%w{ hostname }.each do |cb|
  depends cb
end

depends "hostsfile", "= 1.0.2"
