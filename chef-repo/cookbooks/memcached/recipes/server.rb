#
# Main recipe for Memcached server node:
#

package "memcached" do
  action :upgrade
end

template "/etc/memcached.conf" do
  source "memcached.conf.erb"
  mode 0440
  owner "root"
  group "root"
  notifies :restart, "service[memcached]"
end

service "memcached" do
  supports :restart => true, :reload => true, :stop => true, :start => true
  action :enable
end
