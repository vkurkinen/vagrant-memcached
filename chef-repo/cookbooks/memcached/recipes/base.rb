
#
# Base recipe for all units to use. Installs common packages.
#

# setup apt update
include_recipe "memcached::apt"

# enable hostname and /etc/hosts generation
node.default[:set_fqdn_reload_method] = "reload"
include_recipe "hostname"

# update /etc/hosts to include all host addresses
hosts = node['hosts']
hosts.each do |host|
    hostsfile_entry host[:ip] do
      hostname  host[:name]
      action    :create_if_missing
    end
end


# install basic packages and utils.
package "joe" do
  action :upgrade
end

package "zip" do
  action :upgrade
end

package "unzip" do
  action :upgrade
end



