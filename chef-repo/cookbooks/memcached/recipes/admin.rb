#
# Main recipe for Memcached admin node:
#

#CHECK: may need to run apt-get update in order to get php5 installed..

install_dir = "/opt/app/memcached-admin"

# 1. Install apache2
package "apache2" do
  action :upgrade
end

# NOTE: apt-get update must have run in first deployment to get this installed.
package "libapache2-mod-php5" do
  action :upgrade
end

service "apache2" do
  supports :restart => true, :reload => true, :stop => true, :start => true
  action :enable
end

# 2. Setup memcache-admin site in apache2

template "/etc/apache2/sites-available/memcached-admin" do
  source "memcached-admin-site.conf.erb"
  mode 0440
  owner "root"
  group "root"
  notifies :restart, "service[apache2]"
end

execute "enable-memcached-admin-site" do
   command "/usr/sbin/a2ensite memcached-admin"
   notifies :restart, "service[apache2]"
   not_if { ::File.exists?("/etc/apache2/sites-enabled/memcached-admin")}
end

# 3. Install memcache-admin (by downloading it if needed)

remote_file "#{Chef::Config[:file_cache_path]}/phpMemcachedAdmin.zip" do
  source "http://phpmemcacheadmin.googlecode.com/files/phpMemcachedAdmin-1.2.2-r262.zip"
end

directory "#{install_dir}" do
  owner "www-data"
  group "www-data"
  mode 00755
  recursive true
  action :create
end

execute "install-memcached-admin" do
    command "unzip #{Chef::Config[:file_cache_path]}/phpMemcachedAdmin.zip -d #{install_dir}"
    not_if { ::File.exists?("#{install_dir}/index.php")}
end

directory "#{install_dir}/Temp" do
  owner "www-data"
  group "www-data"
  mode 00777
  notifies :restart, "service[apache2]"
end

#file "#{install_dir}/Config/Memcache.php" do
#  owner "www-data"
#  group "www-data"
#  mode 00777
#  notifies :restart, "service[apache2]"
#end


# Configure servers to admin configuration

servers = node['memcached_servers']
template "#{install_dir}/Config/Memcache.php" do
  source "Memcache.php.erb"
  owner "www-data"
  group "www-data"
  mode 00777
  variables({
     :servers => node[:memcached_servers]
  })
end