name "memcached-server"
description "Role for Memcached server node"

run_list "recipe[memcached::base]","recipe[memcached::server]"
