name "memcached-admin"
description "Role for Memcached admin node"

run_list "recipe[memcached::base]","recipe[memcached::admin]"
