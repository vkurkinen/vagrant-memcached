vagrant-memcached
=================

This module allows instantiating and controlling a memcached server setup for local development
purposes, with one or more server nodes. A simple admin GUI for monitoring statistics
is installed to the first server node so it is possible to view memcached
statistics via browser.

The servers are defined with rather low resources (memory and CPU) as this is 
intended to be used only for local development.

Requirements
============

You need to have a fully working Vagrant 1.4 setup locally in order
to setup the system.

Should work in Ubuntu 13.10 based systems at least with following 
base requirements in place:

* Vagrant version >= 1.4.2
* Virtualbox version >=4.2.16
* Vagrant omnibus plugin
* Ruby >= 1.9.3

To get started, install the basic requirements and ensure that you have 
fully working Vagrant + Virtualbox setup e.g. by creating a simple sample
box with the ``precice64`` template as explained in:

* [Vagrant documentation](http://docs.vagrantup.com/v2/installation/index.html).
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Then install the **vagrant-omnibus** Vagrant plugin. It's needed as we need to 
upgrade Chef inside each VM prior to launching node provisioner
as the node provisioning is based on newer Chef version (mostly due to the 
hostnames cookbook that requires Ruby >=1.9). **vagrant-omnibus** allows 
us to control Chef version in Vagrant file. 

The VM nodes must have access to Internet as they download some of the 
packages from external repositories. 

Usage
==========

Just clone or fork the repository, the then do things mentioned below.

Following 3rd party cookbooks are used:

* [hostname](https://github.com/nickola/chef-hostname)
* [hostsfile](https://github.com/customink-webops/hostsfile)

They are included as submodules so after clone do: 

    $ git submodule init
    $ git submodule update

Upgrading Chef in VMs
----------------------

The deployment and setup of nodes is done with **chef-solo**. 
System requires newer Chef than what comes with the box. In order
to make that possible and to install correct version of Chef to the nodes
before ``vagrant provision``, the [vagrant-omnibus](https://github.com/schisamo/vagrant-omnibus)
plugin needs to be installed to your local Vagrant.

Just execute: 

    $ vagrant plugin install vagrant-omnibus

chef-repo
-----------

Provisioner resources are in ``chef-repo```directory. 

It defines two main roles:

* ``base``: For common server node setup.
* ``memcached-server`` : memcached server setup.
* ``memcached-admin``: memcached admin GUI (phpmemcachedadmin).

Server and admin roles can be defined in same node.
All deployment scripts are in *memcached* cookbook. The recipes
are rather self-explanatory to study them if interested in details.

Vagrantfiles
------------

The actual ``Vagrantfile`` is located under **deployments**, e.g.
**deployments/default/Vagrantfile**. Do all vagrant work in that directory.

Local.yaml
-----------

Optionally, you can define a local configuration file to the same directory where
``Vagrantfile`` is located. The file is called **local.yaml** and it 
has following kind of content:

```
number_of_servers: 2
```

It tells that you want to deploy a two node system. 

If you don't use local file, you can also edit the ``Vagrantfile`` direcly, or
just invoke with default configuration (that is, single node system).

The deployment is really simple and does only the basics needed to get up a 
working memcached setup, so expect some things missing or places for improvements.

What It Does?
=============

During ``vagrant up`` following happens:

* Local override configuration file `local.yaml` is read, if it exists and then
  number of server nodes to be instantiated is resolved. If not local config
  exist, only single node is created.
* Generates configuration for Virtualbox and Chef Solo provisioner for each node (see ``Vagrantfile``).
* Launches Virtualbox VM for each node with ``precise64`` (Ubuntu 12.04) OS.
* Installs and configures local DNS (``/etc/hosts``) to each node.
* Installs and configures memcached server to each node. 
* Installs and configures memcached admin GUI to the first node.

Accessing Nodes
===============

Nodes are named as **memcached-server-N** where N is 1-based index. 
Nodes are configured with private networking, with IPs **192.168.60.1N**.

So, e.g. 3 node system has following nodes: 

* **192.168.60.11 (memcached-server-1)**: A primary memcached server node with admin GUI.
* **192.168.60.12 (memcached-server-2)**: A memcached server node.
* **192.168.60.13 (memcached-server-3)**: A memcached server node.

Within the nodes admin node can also be referenced with name ``memcached-admin``.

The admin node runs in ``apache2`` with [phpmemcachedadmin](https://code.google.com/p/phpmemcacheadmin). Access
it via <http://192.168.60.11/memcached-admin>.

Memcached is accessible via port **11211**.

You can also SSH to nodes from the vagrant directory using:

   $ vagrant ssh <node-name>

Deployment
==========

The whole system can be setup and managed via Vagrant. There 
is one directory for each specific deployment type under the 
`*deployments* folder. The *deployments/default* should be usable as is.

Open shell and go to the directory where ``Vagrantfile`` is located and then
do one of the following:

To create new system:

    $ vagrant up
    
    
To re-configure all servers:

    $ vagrant provision
    
    
To suspend running nodes:

    $ vagrant suspend
   

To resume suspended nodes:

    $ vagrant resume
    

To permanently destroy all nodes:

    $ vagrant destroy -f

