# -*- mode: ruby -*-
# vi: set ft=ruby :

#
# Boot memcache cluster servers + admin node.
#
#
Vagrant.configure("2") do |config|

  # Use vanilla Ubuntu 12.04 for now
  config.vm.box     = "precise64"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  # Install omnibus first: vagrant plugin install vagrant-omnibus
  # You can then simply configure your chef version in the Vagrantfile before
  # doing the provisioning:
  #config.omnibus.chef_version = :latest
  config.omnibus.chef_version = "11.8.2"

  # How many memcached server nodes to generate, must be at least 1.
  # Read from local overrides, if exist
  number_of_servers = 3
  if File.exist?("local.yaml")
     require "yaml"
     p = YAML.load_file( 'local.yaml' )
     if p.has_key?("number_of_servers")
        number_of_servers = p["number_of_servers"]
    end
  end
  number_of_servers = (number_of_servers > 0) ? number_of_servers : 1
  
  # nodes to generate, 1st is always admin.  
  nodes = []
  number_of_servers.times do |n|
     nodes[n] = "memcached-server-#{n+1}"
  end
  admin_node = "memcached-server-1"
  admin_name = "memcached-admin"
  
  # Generate local DNS + other common configs
  dns_hosts          = []
  node_ips           = []
  memcached_servers  = []
  
  nodes.each_with_index do |node,idx|         
      node_ips[idx] = "192.168.60.1#{idx+1}"
      if node == admin_node
         dns_hosts[idx] = { "name" => "#{node} #{admin_name}", "ip"   => node_ips[idx] }
      else
         dns_hosts[idx] = { "name" => node, "ip"   => node_ips[idx] }
      end
      memcached_servers[idx] = { "host" => node, "port" => "11211" }
  end
    
  # Generate actual node VM configs
  nodes.each_with_index do |node_name, idx|  
      config.vm.define node_name, primary: (idx == 0)  do |server|   
         node_ip = node_ips[idx]
         server.vm.hostname = node_name
         server.vm.provider "virtualbox" do |v|
           v.name = node_name
           v.memory = 512
           v.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
           v.customize ["modifyvm", :id, "--cpus", "1"]
           v.customize ["modifyvm", :id, "--cpuhotplug", "off"]
           v.customize ["modifyvm", :id, "--audio", "none"]
           v.customize ["modifyvm", :id, "--usb", "off"]
         end
         
         server.vm.network :private_network, ip: node_ip
         server.vm.provision :chef_solo do |chef|
             chef.cookbooks_path = "../../chef-repo/cookbooks"
             chef.roles_path     = "../../chef-repo/roles"
             chef.data_bags_path = "../../chef-repo/data_bags"
             if node_name == admin_node
                chef.add_role "memcached-admin"
             end
             chef.add_role "memcached-server"
             chef.node_name = node_name
             chef.json = {
                "set_fqdn" => "#{node_name}.local",
                "hosts"    => dns_hosts,
                "memcached_servers" => memcached_servers
             }
          end # server.vm
     end # config.vm
     
  end  # nodes

  # config.vm.share_folder "v-data", "/vagrant_data", "../data"
end
